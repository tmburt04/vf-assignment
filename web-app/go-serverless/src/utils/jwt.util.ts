const TOKEN_URL_REGEX = /(#id_token=)([A-Za-z0-9\._-]{1,})(&)/;

/**
 * @description used to decode and interpret the token received from an auth provider to a more understandable format.
 * @param token the AWS token you are wanting to parse.
 */
export const parseJwt = (token) => {
    if (!!token) {
        try {
            const base64Url = token.split('.')[1];
            const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            const jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
            const payload = JSON.parse(jsonPayload);
            return payload;
        } catch (e) {
            console.warn(e);
        }
    }
    console.warn(`JSON web token not found.`);
}

/**
 * @description fetches the id token from the requesting url and parses it
 * @returns the provided token as well as a profile
 */
export const getSession = () => {
    // Set token if available in local storage
    let key = localStorage.getItem("AUTH_TOKEN");
    let profile;

    // Check if a token has been passed in via URL query Param
    const matches = TOKEN_URL_REGEX.exec(location.href);
    if (!key && matches && matches.length > 2) {
        key = matches[2];
        localStorage.setItem("AUTH_TOKEN", key);
    }

    // Parse token to a readable profile
    profile = parseJwt(key);
    return {
        key,
        profile
    }
}
