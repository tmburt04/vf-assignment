export const environment = {
  production: true,
  awsUrl: "https://4udj3sex4i.execute-api.us-east-1.amazonaws.com/qa/",
  authConfig: {
    hostedUiDomain: "https://vf-demo-qa.auth.us-east-1.amazoncognito.com",
    clientId: "6nnvk7pm2cevq37pe6ie3psmsa",
    responseType: "token",
    scope: "openid",
  }
};
