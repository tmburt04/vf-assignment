import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './route-guards/authorized-user/authorized.guard';

const routes: Routes = [
  {
    path: 'secure',
    canActivate: [AuthGuard], // Block unauthenticated users, redirect them to the auth page if necessary
    loadChildren: () => import('./secure-home/secure-home.module').then(m => m.SecureHomeModule) // Lazy load the secure components
  },
  { path: '', redirectTo: 'secure', pathMatch: 'full' },
  { path: '**', redirectTo: 'secure', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
