import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'dataURI'
})
export class DataURIPipe implements PipeTransform {

    public transform(blob: any) {
        try {
            const uri = URL.createObjectURL(blob);
            return uri;
        } catch (err) {
            console.warn(`Could not build data uri:`, err);
            return '/assets/images/unknown-file.svg';
        }
    }
}