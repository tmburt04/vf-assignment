import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({
    name: 'sanitize'
})
export class SanitizePipe implements PipeTransform {

    constructor(protected sanitizer: DomSanitizer) { }

    public transform(value: any, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
        const _type = type.toUpperCase();
        switch (_type) {
            case 'HTML':
            case 'HTML5':
                return this.sanitizer.bypassSecurityTrustHtml(value);
            case 'STYLE':
            case 'SCSS':
            case 'CSS':
                return this.sanitizer.bypassSecurityTrustStyle(value);
            case 'JS':
            case 'SCRIPT':
            case 'SCRIPTS':
                return this.sanitizer.bypassSecurityTrustScript(value);
            case 'URL':
            case 'URI':
                return this.sanitizer.bypassSecurityTrustUrl(value);
            case 'RESOURCE':
                return this.sanitizer.bypassSecurityTrustResourceUrl(value);
            default:
                throw new Error(`Invalid safe type specified: ${type}`);
        }
    }
}