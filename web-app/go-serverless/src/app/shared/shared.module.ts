import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropUploadDirective } from './drag-drop-upload.directive';
import { SanitizePipe } from './pipes/sanitize.pipe';
import { DataURIPipe } from './pipes/data-uri.pipe';

@NgModule({
  declarations: [DragDropUploadDirective, SanitizePipe, DataURIPipe],
  exports: [DragDropUploadDirective, SanitizePipe, DataURIPipe],
  imports: [
    CommonModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharedModule { }
