import { Directive, HostListener, HostBinding, Output, EventEmitter, Renderer2 } from '@angular/core';
import { UploadService } from '../secure-home/upload/upload.service';

@Directive({
  selector: '[DragDropUpload]'
})
export class DragDropUploadDirective {

  constructor(private uploadService: UploadService) { }

  @HostBinding('style.background-color') public background = 'lightgray';
  @HostBinding('style.opacity') public opacity = '1';

  //Dragover listener
  @HostListener('dragover', ['$event']) onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = 'lightgray';
    this.opacity = '0.8';
  }

  //Dragleave listener
  @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = 'lightgray'
    this.opacity = '1'
  }

  //Drop listener
  @HostListener('drop', ['$event']) public ondrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = 'lightgray'
    this.opacity = '1'
    let files = evt.dataTransfer.files;
    this.uploadService.loadAllFiles(files);
  }

}
