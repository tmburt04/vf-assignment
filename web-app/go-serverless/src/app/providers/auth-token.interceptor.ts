import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { AppService } from './app.service';

const SIGNEDURL_IDENTIFIER = "X-Amz-Signature";

@Injectable()
export class AuthTokenHttpInterceptor implements HttpInterceptor {

    constructor(
        private app: AppService
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const authToken = this.app.token; // Fetch token, if available

        // Attach authorization header to all non-signed urls
        const url = request.url;
        if (!url.includes(SIGNEDURL_IDENTIFIER)) {
        // clone & Re-assign the request because you cannot directly modify the request.
        request = request.clone({ headers: request.headers.set('Authorization', authToken) });
        }

        return next.handle(request).pipe(
            catchError((err: any, caught) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 403 || err.status === 401) {
                        // Logout user if they get an unauthorized response.
                        // this.app.logoutUser();
                        console.log("User is no longer authorized to make this request", err)
                    }
                    return Observable.throw(err);
                }
            })
        );
    }
}