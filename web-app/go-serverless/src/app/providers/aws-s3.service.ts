import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { share, filter, tap, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FileUpload } from '../secure-home/upload/file-upload.model';

class SignedResponse {
  signedURL;
  Key;
}

enum PATHS {
  UPLOAD = "s3/upload"
}

@Injectable({
  providedIn: 'root'
})
export class AwsS3Service {

  constructor(private http: HttpClient) { }

  /**
   * 
   * @param file the instance of FileUpload you want to upload
   */
  uploadFile(file: FileUpload) {
    return this.getSignedURL(file.data.name, file.metadata).pipe(
      switchMap(resp =>
        this.pushObjectToBucket(resp.signedURL, file.data.blob)
        ),
      share()
    ).toPromise();
  }

  private getSignedURL(fileName: string, metadata = {}) {
    const endpoint = `${environment.awsUrl}${PATHS.UPLOAD}`;
    // 'Filter' out falsey values
    // 'tap' the result to the logs
    // 'Share' output values to prevent multiple calls with new subscribers
    return this.http.post<SignedResponse>(endpoint, {
      metadata: {
        fileName,
        ...metadata
      }
    }).pipe(
      filter(v => !!v),
      share()
    );
  }

  /**
   * 
   * @param signedUrl URL Received from AWS, JWT Signed
   * @param file File that we are pushing to S3
   */
  private pushObjectToBucket(signedUrl: string, blob: Blob) {
    // 'Filter' out falsey values
    // 'tap' the result to the logs
    // 'Share' output values to prevent multiple calls with new subscribers
    return this.http.put(signedUrl, blob).pipe(
      filter(v => !!v),
      share()
    );
  }
}