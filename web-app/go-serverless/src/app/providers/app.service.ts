import { Injectable, OnInit } from '@angular/core';
import { getSession } from 'src/utils/jwt.util';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private userToken;
  user;

  // read access for the http interceptor
  get token() {
    return this.userToken;
  }

  // Session validation property for route guards
  get validSession() {
    return !!this.userToken;
  }

  constructor() {
    const { key, profile } = getSession();
    this.user = profile;
    this.userToken = key;
  }

  /**
   * @description Clears all instances of the user and reloads the page (the page should redirect to the Cognito hosted UI)
   */
  logoutUser() {
    localStorage.clear();
    this.user = null;
    this.userToken = null;
    location.reload();
  }
}

