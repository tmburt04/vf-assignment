import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppService } from './providers/app.service';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { SecureHomeModule } from './secure-home/secure-home.module';
import { AuthTokenHttpInterceptor } from './providers/auth-token.interceptor';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    SecureHomeModule,
    BrowserAnimationsModule, // required for Notifications module
    ToastrModule.forRoot(), // For Notifications
  ],
  providers: [AppService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthTokenHttpInterceptor,
      multi: true
    },],
  bootstrap: [AppComponent]
})
export class AppModule { }
