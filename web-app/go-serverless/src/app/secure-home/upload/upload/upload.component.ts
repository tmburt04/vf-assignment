import { Component, OnInit } from '@angular/core';
import { UploadService } from 'src/app/secure-home/upload/upload.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent {

  get files() {
    return this.uploadService.files;
  }

  constructor(protected uploadService: UploadService) {
  }

  /**
   * 
   * @param file Represents the file you are wanting to upload
   */
  async pushToCloud(file) {
    return await this.uploadService.saveToCloud(file);
  }

  /**
   * @description Saves all the files loaded currently to the cloud
   */
  async pushAllToCloud() {
    await this.uploadService.saveAllToCloud();
  }

  /**
   * @description Saves all the files loaded currently to the cloud
   */
  loadFile(files) {
    if (!!files) {
      this.uploadService.loadAllFiles(files);
    }
  }

  /**
   * @description Deletes a file from the current list by its index
   */
  deleteAttachment(index) {
    this.uploadService.removeFile(index);
  }
}
