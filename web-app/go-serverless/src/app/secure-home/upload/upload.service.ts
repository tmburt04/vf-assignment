import { Injectable } from '@angular/core';
import { AwsS3Service } from '../../providers/aws-s3.service';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FileUpload } from './file-upload.model';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  private fileList: any[];

  get files() {
    return this.fileList;
  }

  constructor(protected cloudStorage: AwsS3Service, private toastr: ToastrService) {
    this.fileList = [];
  }

  /**
   * 
   * @param i Index of file you would like to update
   */
  async saveToCloud(i: number) {
    const file: FileUpload = this.fileList[i];
    return file.uploadFile().then(() => {
      this.toastr.success(`${file.data.name} uploaded to the cloud successfully!`);
    }).catch(err => {
      console.warn(err);
      this.toastr.error('Please try removing the file and re-uploading.', `${file.data.name} failed to upload!`);
    })
  }

  async saveAllToCloud() {
    if (!!this.fileList) {
      await this.fileList.map((_file, i) => {
        this.saveToCloud(i);
      })
    }
    return true;
  }

  async loadFile(file) {
    if (!!file) {
      const upload = new FileUpload(this.cloudStorage);
      await upload.initFileUpload(file);
      this.fileList.push(upload);
      this.toastr.info(`${upload.data.name} has been loaded.`);
    }
  }

  loadAllFiles(files: FileList) {
    for (let index = 0; index < files.length; index++) {
      const _file = files[index];
      this.loadFile(_file)
    }
  }

  removeFile(index) {
    if (index != null) {
      const { name } = this.fileList[index];
      this.fileList.splice(index, 1);
      this.toastr.warning(`${name} has been deleted.`);
    }
  }
}
