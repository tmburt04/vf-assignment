import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadComponent } from './upload/upload.component';
import { SharedModule } from '../../shared/shared.module';
import { SanitizePipe } from '../../shared/pipes/sanitize.pipe';
import { DataURIPipe } from '../../shared/pipes/data-uri.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [UploadComponent],
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    SanitizePipe,
    DataURIPipe
  ]
})
export class UploadModule { }
