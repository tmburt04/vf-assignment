import { AwsS3Service } from 'src/app/providers/aws-s3.service';

const B64_MIME_REGEX = /(data:)([a-zA-Z0-9]+\/[a-zA-Z0-9\-.+]+)(;base64,)(.*)/;
const MAX_KB_DYNAMO = 400;
const BYTES_IN_KB = 1024;
const MAX_BYTES_IN_DYNAMO = MAX_KB_DYNAMO * BYTES_IN_KB;

export class FileUpload {

    private name: string;
    private type: string;
    private id: string;
    private blob: Blob;
    private isUploaded: Boolean;
    private _metadata;
    private _size;
    
    // Get size of file to determine compatibility
    get size() {
        return this._size;
    }
    
    // Determine whether this instance meets the size requirements for dynamodb
    get dbCompatible() {
        return this._size < MAX_BYTES_IN_DYNAMO;
    }

    // Abstract away the setting of meta data just in case 
    set description(str) {
        this._metadata.description = str;
    }
    get description() {
        return this._metadata.description;
    }


    // Quick property read determining whether the file has been uploaded or not.
    get inTheCloud() {
        return this.isUploaded;
    }

    // Returns any metdata set about the file
    get metadata() {
        return this._metadata;
    }

    /**
     * 
     * @param successful whether the attempt was successful
     * @description Used to log attempts to upload.  Could be expanded to log the number of attempts.
     */
    private logAttempt(successful = false) {
        this.isUploaded = successful;
    }

    /**
     * @description Interfaces with the cloud provider service to push this instance of fileUpload to the cloud
     */
    async uploadFile() {
        try {
            console.log(`Uploading file...`, this.name);
            return await this.cloudStorage.uploadFile(this).then(() => {
                this.logAttempt(true);
                console.log(`Upload of ${this.name} successful.`);
                return true;
            });
        } catch (err) {
            this.logAttempt();
            console.warn(`Upload Failed.`, err);
            return Promise.reject();
        }
    }

    get data() {
        return {
            name: this.name,
            type: this.type,
            id: this.id,
            blob: this.blob
        };
    }

    constructor(private cloudStorage: AwsS3Service) {
        this.id = this.newUUID();
        this._metadata = {};
    }

    /**
     * 
     * @param file the object directly received by the file input element to be uploaded to the cloud
     * @description Initializes the upload object and preps it for the cloud.
     */
    async initFileUpload(file) {
        const { name, type, size } = file;
        this.name = name;
        this.type = type;
        this._size = size;
        return await this.parseFileInput(file).then(({ blob }) => {
            this.blob = blob;
            return this.data;
        });
    }

    /**
     * @description Opens a reader to parse and convert the data received from base64 to a Blob
     */
    private parseFileInput = async (file) => {
        try {
            const reader = new FileReader();
            return new Promise((resolve, reject) => {
                reader.onerror = () => {
                    reader.abort();
                    reject(new DOMException("Problem parsing input file."));
                };
                reader.onload = () => {
                    const data = reader.result;
                    const { blob } = this.b642Blob(data);
                    resolve({ blob });
                };
                reader.readAsDataURL(file);
            });
        } catch (err) {
            console.warn(err);
            return Promise.reject();
        }
    }

    /**
     * 
     * @param b64 The data for the file needs to be converted to a blob
     * @description Used to convert a Base64 file to a Blob
     * @returns A blob and its contentType
     */
    private b642Blob = (b64: string | ArrayBuffer) => {
        if (!!b64 && typeof b64 === 'string') {
            const decoded = B64_MIME_REGEX.exec(b64);
            if (decoded && decoded.length > 4) {
                const contentType = decoded[2];
                const content = decoded[4];
                const byteChars = atob(content);
                const byteNums = new Array(byteChars.length);
                for (let i = 0; i < byteChars.length; i++) {
                    byteNums[i] = byteChars.charCodeAt(i);
                }
                const byteArr = new Uint8Array(byteNums);
                const blob = new Blob([byteArr], { type: contentType });
                return {
                    blob,
                    contentType
                }
            } else {
                throw `Base64 value could not be parsed for uploading.`;
            }
        }
    }

    /**
     * @description Generates a new Universally Unique Identifier for files
     */
    private newUUID = () => {
        return `xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx`.replace(
            /[xy]/g,
            c => {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
    }
}