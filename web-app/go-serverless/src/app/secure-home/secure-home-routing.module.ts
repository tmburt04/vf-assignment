import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home/home.component';
import { UploadComponent } from './upload/upload/upload.component';
import { CaseStudyComponent } from './case-study/case-study/case-study.component';


const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'upload-file',
    component: UploadComponent
  },
  {
    path: 'case-study',
    component: CaseStudyComponent
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecureHomeRoutingModule { }
