import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecureHomeRoutingModule } from './secure-home-routing.module';
import { UploadModule } from './upload/upload.module';
import { HomeModule } from './home/home.module';
import { CaseStudyModule } from './case-study/case-study.module';

@NgModule({
  imports: [
    UploadModule,
    HomeModule,
    CaseStudyModule,
    CommonModule,
    SecureHomeRoutingModule
  ]
})
export class SecureHomeModule { }
