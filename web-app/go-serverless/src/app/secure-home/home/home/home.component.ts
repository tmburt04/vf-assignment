import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  assignment = {
    description: "Build a serverless framework (serverless.com) deployment that creates a Lambda, an S3 bucket, and a Dynamo DB table and uploads a file to your bucket. Then, write a plugin that invokes the Lambda after the deployment, extracts data from the file in S3 and inserts that data into DynamoDB.",
    userAcceptance: [
      "File Upload AWS S3 Bucket",
      "Backup DynamoDB Instance",
      "File Upload Lambda Function",
      "Event Lambda on S3 Object creation",
    ]
  }
}
