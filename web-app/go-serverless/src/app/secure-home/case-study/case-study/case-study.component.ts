import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-case-study',
  templateUrl: './case-study.component.html',
  styleUrls: ['./case-study.component.scss']
})
export class CaseStudyComponent {

  technology = [{
    title: "Web Application",
    technologies: [
      {
        'label': "Angular",
        'url': "https://angular.io/",
        'reasonList': [
          "Quick Developemnt",
          "'Out of the box' Utilities"
        ]
      },
      {
        'label': "Bootstrap",
        'url': "https://getbootstrap.com/",
        'reasonList': [
          "CSS Based",
          "Limited Additional JS",
        ]
      }
    ]
  }, {
    title: "API",
    technologies: [
      {
        'label': "AWS Lambda",
        'url': "https://aws.amazon.com/lambda/",
        'reasonList': [
          "Rapid Developemnt",
          "Truely Serverless",
          "Low overhead"
        ]
      }
    ]
  }, {
    title: "Storage",
    technologies: [
      {
        'label': "DynamoDB",
        'url': "https://aws.amazon.com/dynamodb/",
        'reasonList': [
          "Robust",
          "Forgiving",
          "Scalable",
        ]
      },
      {
        'label': "S3",
        'url': "https://aws.amazon.com/s3/",
        'reasonList': [
          "Cheap",
          "Fast",
          "Scalable",
        ]
      }
    ]
  }, {
    title: "Auth",
    technologies: [
      {
        'label': "Cognito",
        'url': "https://aws.amazon.com/cognito/",
        'reasonList': [
          "Hosted Authentication User Interface",
          "Direct Integration w/Lambda",
          "Pre-configured User Management",
        ]
      }
    ]
  }
];
}
