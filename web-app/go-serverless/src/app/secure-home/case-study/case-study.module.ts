import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaseStudyComponent } from './case-study/case-study.component';



@NgModule({
  declarations: [CaseStudyComponent],
  imports: [
    CommonModule
  ]
})
export class CaseStudyModule { }
