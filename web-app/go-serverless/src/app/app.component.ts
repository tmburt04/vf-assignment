import { Component } from '@angular/core';
import { AppService } from './providers/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private app: AppService) {
  }

  logoutUser() {
    this.app.logoutUser();
  }
}
