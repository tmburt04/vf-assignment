import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/providers/app.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private service: AppService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const isValid = this.service.validSession;
    // Redirect user to hosted ui if they do not have a valid session
    if (!isValid) {
      console.warn("Session is not valid, redirecting...");
      const redirectURI = window.location.origin;
      const config = environment.authConfig;
      const hostedUiUrl = `${config.hostedUiDomain}/login?client_id=${config.clientId}&response_type=${config.responseType}&scope=${config.scope}&redirect_uri=${redirectURI}`;
      window.location.href = hostedUiUrl;
    }
    return isValid;
  }
}
