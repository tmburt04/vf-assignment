import 'source-map-support/register';
import { putItem } from './utils/AWS/database.util';
import { getRawObject } from './utils/AWS/storage.util';

/**
 * @param event An S3Event triggered on creation of an Object in an S3 bucket.
 * @description An S3 Event function triggered on creation of an Object in an S3 bucket. When triggered, it should iterate over the object(s) 'PUT' in the S3 Bucket backing each one up to a DynamoDB Table
 */
export const onUpload = async (event) => {
    try {
        // Extract the objects pushed to S3
        const { Records } = event;

        // Iterate over each Object
        const uploads = await Promise.all(Records.map(async ({ s3 }) => {
            const {
                bucket: { name },
                object: { key }
            } = s3;
            // Retrieve the whole object stored in Storage
            const objResult = await getRawObject(key, name);
            console.log("Object Retrieved:", objResult);
            // Update Database table with extracted content from Storage
            const result = await putItem({ ...objResult, objKey: key });
            console.log(`An object (id: ${key}) has been pushed to the database.`);
            return result;
        }));

        return {
            statusCode: 200,
            body: JSON.stringify({
                uploads,
            }, null, 2),
        }
    } catch (err) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: err,
                input: event,
            }, null, 2),
        }
    }
};