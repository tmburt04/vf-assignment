import 'source-map-support/register';
import { getUploadURL } from './utils/AWS/storage.util';

/**
 * @param event 
 * @description A HTTP Lambda function triggered over REST, that generates a pre-authorized URL to update or create a object in S3. 
 */
export const uploadFile = async (event) => {
  try {
    const body = JSON.parse(event.body);
    const metadata = body.metadata;
    // Destruct the signed response to extract the pre-authorized url and key of the object
    const { signedURL, Key } = await getUploadURL(metadata);

    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type,Accept,Authorization'
      },
      body: JSON.stringify({
        signedURL,
        Key
      }, null, 2),
    }
  } catch (err) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: err,
        input: event,
      }, null, 2),
    }
  }
}
