import 'source-map-support/register';
import * as uuid from 'uuid';
const AWS = require('aws-sdk');
const s3Config = require('../../../config/s3.config.json');
const s3 = new AWS.S3(s3Config);
const UPLOAD_BUCKET = process.env.UPLOAD_BUCKET;

/**
 * 
 * @param Metadata Any additional information you want to include about the object you are storing
 * @param Bucket The Bucket that you are updating the Object in. (Defaults to the Bucket name defined in our serverless file)
 * @description An interface with AWS' S3 SDK to add or update an object in a S3 Bucket.
 * @returns A preauthorized url to update/create an object, along with the key for future reference.
 */
export const getUploadURL = async (Metadata = {}, Bucket = UPLOAD_BUCKET) => {
    const Key = uuid.v4();
    try {
        console.log(`Generating an authorized url to create an object (${Key}) in the bucket ${UPLOAD_BUCKET}`);
        const signedURL = await s3.getSignedUrl('putObject', {
            Bucket,
            Key,
            Metadata,
            Expires: 60 * 10, // 10 min expiration
        });
        console.log(`Authorized url successfully generated`);
        return {
            signedURL,
            Key
        };
    } catch (err) {
        console.error(`ERROR: Could not generate signed url to create the object (${Key}) in the bucket ${Bucket}:`, JSON.stringify(err));
        throw err;
    }
}

/**
 * @param Key The Key of the object that you want to retrieve the contents from
 * @param Bucket The Bucket that you are reading the Object from. (Defaults to the Bucket name defined in our serverless file)
 * @description An interface with AWS' S3 SDK to explicitly read the contents of a file directly.
 * @returns b64 | Blob
 */
export const getRawObject = async (Key, Bucket = UPLOAD_BUCKET) => {
    try {
        console.log(`Retrieving an object (${Key}) from the bucket ${UPLOAD_BUCKET}`);
        return await s3.getObject({
            Bucket,
            Key
        }).promise();
    }
    catch (err) {
        console.error(`Could not retrieve object (${Key}) from the bucket ${Bucket}:`, err);
        throw err;
    }
};
