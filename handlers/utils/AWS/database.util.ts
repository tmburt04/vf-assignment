import 'source-map-support/register';
const AWS = require("aws-sdk");
const DATABASE_NAME = process.env.DATABASE_NAME;
const dynamoConfig = require('../../../config/dynamo.config.json');
const dynamoDB = new AWS.DynamoDB.DocumentClient(dynamoConfig);

/**
 * @param Item The Object to be added to the DynamoDB table.
 * @param TableName The DynamoDB table you are adding the Object to (Defaults to the table name defined in our serverless file)
 * @description An interface with AWS' DynamoDB DocumentClient to add or update an object in a database table.
 */
export const putItem = async (Item, TableName = DATABASE_NAME) => {
    try {
        console.log(`Pushing an object to the database table ${TableName}...`);
        const result = await dynamoDB.put({
            TableName,
            Item
        }).promise(); // Return a promise instead of a callback
        console.log(`Object successfully stored in the database table ${TableName}.`, JSON.stringify(result));
        return result;
    } catch (err) {
        console.log(`Object could not be stored in the database table ${TableName}.`, JSON.stringify(err));
        throw err;
    }
};