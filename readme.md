# Go Serverless Project
## Description
Build a serverless framework (serverless.com) deployment that creates a Lambda, an S3 bucket, and a Dynamo DB table and uploads a file to your bucket. Then, write a plugin that invokes the Lambda after the deployment, extracts data from the file in S3 and inserts that data into DynamoDB.

## Configuration
### Prerequisites
- AWS Programmatic access
- npm pre-configured
- Serverless CLI
- Angular CLI (if you are using the frontend)

### API
1. Install all dependencies for the project (`npm i` or `npm run build`)
2. Deploy the solution (`npm run deploy:qa`)

This process might take a few moments. However, when complete, the following resources will be generated through serverless:
- An S3 Event Lambda thats only triggered on Upload to the specified Bucket
- A Rest Lambda that consumes a POST, returning a preauthorized 'PUT' URL
- POLP Roles for both Lambdas
- A DynamoDB Bucket for backing up uploaded files
- An S3 Bucket for File Uploads, with a matching CDN
- A pre-configured Static Hosting S3 Bucket, with a matching CDN
- A Cognito UserPool 
- A Cognito UserPoolClient
- A Hosted User Interface through Cognito
- An Authorizer that allows only Cognito users to use a specified resource

> NOTE: Although a Bucket exists for the frontend of this solution, the deployment process for the frontend was purposefully kept separate for maintenance purposes.

### Web App
1. **Ensure the Angular CLI is available in your terminal** (`ng --help`)
2. Navigate to the following directory: ServerlessAssignment/web-app/go-serverless
3. Install all dependencies for the project (`npm i` or `npm run build`)
4. Serve the application locally (`ng serve`)
> NOTE: If you have not deployed the API Infrastructure, the application will not run as intended.
> As it is now, the frontend points to an instance running on my (timmichaelburton@gmail.com) instance running in the cloud. To change this, evaluate the following environment variables:
```
export const environment = {
  ...
  awsUrl: "https://XXXXXXXXXX.execute-api.us-east-1.amazonaws.com/qa/",
  authConfig: {
    hostedUiDomain: "https://XXXXXXXXX.auth.us-east-1.amazoncognito.com",
    clientId: "XXXXXXXXXXXXXXXXXXXXXXXXXX",
    responseType: "token",
    scope: "openid",
  }
};
```

5. To Deploy, simply run the command `npm run hard-deploy:qa`
> NOTE: This command will move the built application (in the 'dist' directory) to the provisioned S3 Bucket configured by the API solution



## Gotchas
1. DynamoDB has a systematic size restriction of 400kb for each Item. Because of this, large files could not be moved unless they are stored in chunks. (https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Limits.html)